(ns ccp.test.core
  (:use [clojure.test]
        [midje sweet cascalog]
        [cascalog.api :only [memory-source-tap ??- <-]]
        [ccp.core]))


(def data-file "data/test.txt")

#_(def logs-tap (??- (etl-logs data-file)))

(def logs-test-data
  [
["2013-06-02T00:00:00-08:00" "fe354816-620f-49f8-a43f-b301763c336c" 25479122 "Home" {:popularItems ["37830" "13183" "23087" "28572" "16193"], :recommendedItems ["11696" "35132" "29981" "33168" "20042"], :recentItem ["21639"]} "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 Safari/8536.25" "4acf6f7" "9982bc5e:facdc79a"]
["2013-06-02T00:00:00-08:00" "485c4b5d-d079-4b30-9d96-1a768f09871c" 5375442 "Play" {:itemId "13650", :marker 2851} "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.15) Gecko/20110303 Ubuntu/10.04 (lucid) Firefox/3.6.15" "d5c29d3b" "4dd15a36:cbd3945d"]
["2013-06-02T00:00:00-08:00" "1f3b4198-e476-416c-9db2-b7ff9f03fce7" 65021912 "Play" {:itemId "34645", :marker 3300} "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.53 Safari/536.5" "6da5e275" "31ea12ca:1550379d"]
["2013-06-02T00:00:00-08:00" "219f2cd8-f52b-417a-ae8a-ab0e6710098b" 30115478 "Play" {:itemId "10375", :marker 3540} "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:8.0) Gecko/20100101 Firefox/8.0" "8a5919f5" "f36fe47d:10b9150"]
   ]
)


(def action-data-test [[25479122 "fe354816-620f-49f8-a43f-b301763c336c" 1370160000 "Home" "-" -1]
             [5375442 "485c4b5d-d079-4b30-9d96-1a768f09871c" 1370160000 "Play" "13650" 2851]
             [65021912 "1f3b4198-e476-416c-9db2-b7ff9f03fce7" 1370160000 "Play" "34645" 3300]
             [30115478 "219f2cd8-f52b-417a-ae8a-ab0e6710098b" 1370160000 "Play" "10375" 3540]])

;;TODO create tmp test directory or mock it
#_(deftest etl-logs-test
  (let [fields []
        in [[]]
        out [[]]]
    (fact
     (etl-logs "TODO")
     => (produces out))))

(deftest data->timestamp-test
  (is (= 1370160000 (date->timestamp "2013-06-02T00:00:00-08:00") )))

(deftest missing-keys-count-test
  (let [in [[{ :createdAt "-" :sessionId "-" :user "-" :type "-" :payload "-" :userAgent "-" :refId "-"}]
            [{ :sessionId "-" :user "-" :type "-" :payload "-" :userAgent "-" :auth "-"}]
            [{:createdAt "-" :sessionId "-" :user "-" :type "-" :payload "-" :userAgent "-" :refId "-" :auth "-"}]]]
    (fact (missing-keys-count in)
          => (produces [["auth" 1] ["createdAt-refId" 1]]))))

(deftest action-details-test
  (let [in logs-test-data
        out action-data-test]
    (fact
     (action-details (memory-source-tap log-fields in))
     => (produces out))))

(deftest action-counts-test
  (let [fields ["?user" "?session" "?timestamp" "!type" "?arg1" "?arg2"]
        in action-data-test
        out [[5375442 "485c4b5d-d079-4b30-9d96-1a768f09871c" 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0] [25479122 "fe354816-620f-49f8-a43f-b301763c336c" 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0] [30115478 "219f2cd8-f52b-417a-ae8a-ab0e6710098b" 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0] [65021912 "1f3b4198-e476-416c-9db2-b7ff9f03fce7" 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]]
        ;;
        [user session] [1111 "session-id"]
        in1 (vec             
             (map (fn [row] (vec (concat [user session] (drop 2 row))))
                  action-data-test))
        out1 [[user session 4 0 0 1 0 0 0 0 0 3 0 0 0 0 0 0 0 0]]]
    (fact
     (action-counts (memory-source-tap fields in))
     => (produces out)
     (action-counts (memory-source-tap fields in1))
     => (produces out1)
     )))

(def session1 [
               [1135848 "7b4735cb" 1370160419 "Play" 36508   5520]
               [1135848 "7b4735cb" 1370160479  "Play" 36508   5580]
               [1135848 "7b4735cb" 1370160539  "Play" 36508   5640]
               [1135848 "7b4735cb" 1370160599 "Play"  36508   5700]
               
               ])

(deftest play-duration-test
  (let [
        in (mapv #(mapv str %) session1)
        out [["1135848" "7b4735cb" "36508" 180 240 1370160419 5520 5700]]]
    (fact
     (play-duration in)
     => (produces out))))

(deftest kid-user-test
  (let [fields []
        in [
            [ 51082221  "12c787a9"   1370225308  "Account" "adult"  "kid"]
            [ 83972235  "097e9bf6"   1370226850  "Account" "adult"  "kid"]
            [ 90457127  "ef635236"   1370227102  "Account" "adult"  "kid"]
            [ 8731494   "395e3bfd"   1370227620  "Account" "adult"  "kid"]
            [ 93226529  "ea0cb34a"   1370228483  "Account" "adult"  "kid"]
            [ 57233115  "4b873f23"   1370228537  "Account" "adult"  "kid"]            
            ]
        out [[8731494 1370227620 -1]
             [51082221 1370225308 -1]
             [57233115 1370228537 -1]
             [83972235 1370226850 -1]
             [90457127 1370227102 -1]
             [93226529 1370228483 -1]]]
    (fact
     (kid-user in)
     => (produces out))))

(deftest between?-test
  (is (= true (between? 1 -1 2)))
  (is (= true (between? 5 3 -1)))
  (is (= false (between? 5 -1 2)))
  (is (= true (between? 1 nil 2)))
  (is (= true (between? 1 -1 nil))))

(deftest kid-films-test
  (let [;fs1 [] fs2 []
        ;; user ,session , ts , type , arg1 arg2
        action-in [[8731494 "fe354816" 1370160000 "Home" "-" -1]
             [5375442 "485c4b5d" 1370160000 "Play" "13650" 2851]
             [8731494 "1f3b4198" 1370160000 "Play" "34645" 3300]
             [57233115 "219f2cd8" 1370160000 "Play" "10375" 3540]]
        ;; user , start-ts , end-ts
        user-in [[8731494 1370027620 -1]
             [57233115 1370028537 -1]]
        ;;film , user
        out [["10375" 1] [ "34645" 1]]]
    (fact
     (kid-films user-in action-in)
     => (produces out))))

(deftest split-films-test
  (fact (films-split [[1 23] [3 45]] 
                       [[10011877 "721" 1 840  900  1370203223 0 840]
                        [10011877 "721" 3 840  900  1370203223 0 840]           
                        [10011877  "721" 2 840  900  1370203223 0 840]])
        => (produces [[10011877 3 2]])))

(deftest interval-query-test
  (let [in [["id1" "start" 10]["id2" "start" 100]["id1" "end" 120]["id3" "end" 150]]
        out [["id1" 10 120] ["id2" 100 -1] ["id3" -1 150]]
        q (<- [?id !!start !!end]
              (default !!start -1 :> ?strt)
              (default !!end -1 :> ?nd)
              ((<- [?id ?ts]
                   (= "start" ?type)
                   (in ?id ?type ?ts)) :> ?id !!start)
              ((<- [?id ?ts]
                   (= "end" ?type)
                   (in ?id ?type ?ts)) :> ?id !!end))]))

(deftest session-stats-test
  (let [fields []
        in [
            [51082221    "12c787a9-9a86"    1370225265  "Home"    "-"   -1]
            [51082221    "12c787a9-9a86"    1370225273  "Hover"   "16177"   -1]
            [51082221    "12c787a9-9a86"    1370225280  "Hover"   "26237"   -1]
            [51082221    "12c787a9-9a86"    1370225289  "AddToQueue"  "-"   -1]
            [51082221    "12c787a9-9a86"    1370225290  "VerifyPassword"  "-"   -1]
            [51082221    "12c787a9-9a86"    1370225308  "Account" "adult"   "kid"]
            ]
        out [[51082221 "12c787a9-9a86" 43 6]]]
    (fact
     (session-stats in)
     => (produces out))))

(deftest ratings-test
  (fact
   (ratings [["a" "b" 23 "Rate" "1234e2" 4]])
   => (produces ["a" "123" 4])))

(deftest pairs-test
  (is (= [[1 2] [1 3] [2 3]] (pairs [1 2 3])))
  (is (= [["aa" "bb"] ["aa" "cc"] ["bb" "cc"]] (pairs ["aa" "bb" "cc"]))))

(deftest items-relation-test
  (let [;;user, session, item, duration1, duration2, min-t, min-m, max-m
        in [
            [10011877    "721bbbbf"    "9722e26" 840 900 0 0  840]
            [10016008    "dfe14ccb"    "25792"   5171    5340 0   0   5253]
            [10016008    "dfe14ccb"    "2671"    4800    4860  0  0   4800]
            [10016008    "dfe14ccb"    "28895"   30    3660  0  0   3600]
            [10016008    "dfe14ccb"    "31183"   2311    2400 0   0   2298]
            ]
        ;;user, session, item1, item2
        out [[ 10016008 "dfe14ccb" "25792" "31183"]
             [ 10016008 "dfe14ccb" "25792" "2671"]
             [ 10016008 "dfe14ccb" "2671" "31183"]]
        ]
    (fact
     (items-relation in)
     => (produces out))))
