(ns ccp.myrrix
  (:import
   [net.myrrix.client ClientRecommender MyrrixClientConfiguration]
   [net.myrrix.online.eval AUCEvaluator EvaluationResult]
   [java.io File]
   ))

;;
;; ## Myrrix client : recommendation and clustering
;;
;;   Requirements :
;;   - Run http server :
;; `java -jar myrrix/myrrix-serving-1.0.1.jar --localInputDir myrrix/data --port 8080`
;;   - Ingest some data from web GUI 
;; 

(defn recommender [host & {:keys [port] :or {port 8080}}]
  (ClientRecommender.
   (doto (MyrrixClientConfiguration.)
     (.setHost host)
     (.setPort port))))

(defn estimate [recommender coll]
  (for [[user item] coll]
      [user item (Math/round (.estimatePreference recommender user item))]))

;;
;; ## Myrrix Evaluation
;;

(defn evaluate [in]
  (.evaluate (AUCEvaluator.) (File. in) 0.9 0.5))



