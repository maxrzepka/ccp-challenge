(ns ccp.job
  (:use [ccp core util]
        [cascalog.checkpoint :only (workflow)])
  (:require [ccp.mahout :as ccma]
            [ccp.myrrix :as ccmy]
            [cascalog.api :as ca]            
            [cascalog.lzo :as lzo]
            [cascalog.more-taps :as cmt]))
;;
;; # Hadoop Jobs 
;;
;; ## Introduction
;;
;; To run a clojure REPL
;; hadoop jar ccp.jar clojure.main


;; Manual steps
;; tar.gzip -->  tar xzfO tar.gzip | gzip > big.txt.gz
;;
;; gzip file -> lzo-textline --ETL-> lzo-seqfile --> various counts and
;; action details

;;
;; ## Some basic workflow definitions
;;
(ca/defmain LZOETL [dir out]
  (workflow ["/tmp/ccp-checkpoint"]
            lzo-convert ([:tmp-dirs lzo-path]
                           (ca/?<- (lzo/hfs-lzo-textline lzo-path :sinkmode :replace)
                                   [?line]
                                   ((ca/hfs-textline dir) ?line)))
            total ([]
                     (ca/?- (cmt/hfs-delimited (str out "/total") :sinkmode :replace)
                            (full-count (lzo/hfs-lzo-textline lzo-path))))
            logs ([:deps lzo-convert
                  :tmp-dirs log-path]
                       (ca/?- "clj log"
                              (ca/hfs-seqfile log-path :sinkmode :replace)
                              (logs (lzo/hfs-lzo-textline lzo-path)
                                        :trap (ca/hfs-textline (str out "/trap")))))
            missing-count ([:deps logs]
                       (ca/?- "Count missing keys"
                              (cmt/hfs-delimited (str out "/missingcount") :sinkmode :replace)
                              (missing-keys-count (ca/hfs-seqfile log-path))))
            missing-logs ([:deps logs]
                            (ca/?-
                             (ca/hfs-textline (str out "/missinglogs"))
                             (missing-keys-logs (ca/hfs-seqfile log-path))))
            log-details ([:deps logs]
                         (ca/?- "log details"
                                (cmt/hfs-delimited (str out "/details")
                                                   :sinkmode :replace)
                                (log-details (ca/hfs-seqfile log-path))))
            totalbis ([:deps log-details]
                     (ca/?- (cmt/hfs-delimited (str out "/totalbis") :sinkmode :replace)
                            (full-count (ca/hfs-textline (str out "/details")))))
            ))

(ca/defmain ETLtotals [dir out]
  (workflow ["/tmp/ccp-checkpoint"]
            total ([]
                     (ca/?- (cmt/hfs-delimited (str out "/total") :sinkmode :replace)
                            (full-count (ca/hfs-textline dir))))            
            logs ([:tmp-dirs log-path]
                    (ca/?- "clj log"
                           (ca/hfs-seqfile log-path :sinkmode :replace)
                           (logs (ca/hfs-textline dir)
                                 :trap (ca/hfs-textline (str out "/trap")))))
            total1 ([:deps logs]
                        (ca/?- (cmt/hfs-delimited (str out "/total1") :sinkmode :replace)
                               (full-count (ca/hfs-seqfile log-path))))
            log-details ([:deps logs]
                           (ca/?- "log details"
                                  (cmt/hfs-delimited (str out "/details")
                                                     :sinkmode :replace)
                                  (log-details (ca/hfs-seqfile log-path))))
            total2 ([:deps log-details]
                      (ca/?- (cmt/hfs-delimited (str out "/total2") :sinkmode :replace)
                             (full-count (ca/hfs-textline (str out "/details")))))
            ))

;;
;; ## Misc Jobs
;;
(ca/defmain misc
  "Calls any cascalog query defined in ccp.core with 1 input and 1 output."
  [name in out & {:keys [is os mode]
                  :or {mode :hfs is cmt/hfs-delimited os cmt/hfs-delimited}}]
  (let [
        job (eval (symbol (str "ccp.core/" name)))]
    (ca/?- (os out :sinkmode :replace) (job (is in)))))

(ca/defmain ETLStats
  "Main Fn executing all queries needed for further analysis."
  [dir out]
  (let [log-stage "/tmp/ccp-checkpoint/log" ;(str out "/log")
        details-stage (str out "/details")
        trap (str out "/error")]
   (ca/?- "Full count"
          (cmt/hfs-delimited (str out "/total") :sinkmode :replace)         
          (full-count (ca/hfs-textline dir)))
   (ca/?- "ETL log"
          (ca/hfs-seqfile log-stage :sinkmode :replace)
          (etl-logs (ca/hfs-textline dir) :trap (ca/hfs-textline trap)))
   (ca/?- "Type count"
          (cmt/hfs-delimited (str out "/type") :sinkmode :replace)         
          (type-count (ca/hfs-seqfile log-stage)))
   #_(ca/?- "Missing count"
          (cmt/hfs-delimited (str out "/missing") :sinkmode :replace)         
          (missing-keys-count (ca/hfs-seqfile log-stage)))
   (ca/?- "action details"
          (cmt/hfs-delimited details-stage :sinkmode :replace)
          (action-details (ca/hfs-seqfile log-stage)))
   (ca/?- "session stats"
          (cmt/hfs-delimited (str out "/session") :sinkmode :replace)
          (session-stats (cmt/hfs-delimited details-stage)))
   (ca/?- "Session count"
          (cmt/hfs-delimited (str out "/totalsession") :sinkmode :replace)         
          (full-count (ca/hfs-textline (str out "/session"))))   
   (ca/?- "play duration"
          (cmt/hfs-delimited (str out "/play") :sinkmode :replace)
          (play-duration (cmt/hfs-delimited details-stage)))
   (ca/?- "kids"
          (cmt/hfs-delimited (str out "/kid") :delimiter "," :sinkmode :replace)
          (kid-user (cmt/hfs-delimited details-stage)))
   (ca/?- "kid films"
          (cmt/hfs-delimited (str out "/kidfilm") :sinkmode :replace)
          (kid-films (cmt/hfs-delimited (str out "/kid") :delimiter ",")
                    (cmt/hfs-delimited details-stage)))
   (ca/?- "items relation"
          (cmt/hfs-delimited (str out "/itemrel") :sinkmode :replace)
          (items-relation (cmt/hfs-delimited (str out "/play"))))))

;;
;; ## Workflow definitions : ETL, classify, cluster, recommend
;;
;; TODO investigate if https://github.com/Prismatic/plumbing or other tools
;; can be to better express workflow.
;;

(ca/defmain ETL
  "Generate log details and trap any errors"
  [dir out trap]
  (workflow ["/tmp/ccp-checkpoint"]
            logs ([:tmp-dirs log-path]
                    (ca/?- "clj log"
                           (ca/hfs-seqfile log-path :sinkmode :replace)
                           (logs (ca/hfs-textline dir)
                                 :trap (ca/hfs-textline trap))))
            log-details ([:deps logs]
                           (ca/?- "log details"
                                  (cmt/hfs-delimited out :sinkmode :replace)
                                  (log-details (ca/hfs-seqfile log-path))))))


(ca/defmain classification
  "Given log details source, classifies users between kid and adult
with the following stages :

   - kid : Detects kid users.
   - kid-film : Extracts kid films (ie films watched by kids).
   - count: Count kid films viewed by user.
   - classify: Mark user as kid if high proportion of kid films viewed.
   - export: csv file (user id,0/1) where 0 means Adult, 1 Kid.
"
  [& {:strs [in out duration kid-film lower mode] :or {lower 0.7 mode :distributed}}]
  (let [tap ({"local" cmt/lfs-delimited} mode cmt/hfs-delimited)
        src (when in (tap in))
        src1 (when src (kid-user src))
        src2 (if kid-film (tap kid-film) (kid-films src src1))
        src3 (if duration (tap duration) (play-duration src))
        all-in-one (kidadult-classify src2 src3 :lower lower)]
    (ca/?- (tap out :sinkmode :replace) all-in-one)))

(ca/defmain clustering
  "Given log details source, cluster all sessions in 2 steps :
   - Compute session stats.
   - Run Mahout Clustering where session stats are the feature.
"
  [& {:strs [in out stats t1 t2] :or {stats "/tmp/ccp/clustering"}}]
  (let [mode (if (dfs? in) :dist :local)
        t (when in (tap mode :delimited))]
    (do
      (ca/?- (t stats :sinkmode :replace)
             (session-stats (t in)))
      (if (and t1 t2)
        (ccma/run-clustering stats out :t1 t1 :t2 t2)
        (ccma/run-clustering stats out))
      )))


(ca/defmain recommendation
  "Given log details source, estimate rating for given (user,item) rateme file
Here the following steps :
   - Extracts raw data (user id,film id,rating).
   - TODO Build stage : update myrrix back-end (manual workaround : Ingest data from web GUI)
   - Estimate preferences of rateme input.
   - Export csv file same as test one with one more column rating.
"
  [& {:strs [in csv out rateme tmp mode host]
    :or {host "localhost" out "/tmp/ccp/recommendation"}}]
  (do
    ;;Extract stage if in is provided    
    (when in
     (let [t (tap (if (dfs? in) :dist :local) :delimited)]
       (ca/?- (t out :sinkmode :replace)
              (ratings (t in)))))
   ;;Estimate and export stages when test file is provided
   ;; and supposing myrrix server running on host
   (when rateme
     (coll->file
      (ccmy/estimate (ccmy/recommender host)
                     (file->coll rateme :converter ->number))
      csv))))
