(ns ccp.core
  (:require [clojure.data.json :as json]
            [clojure.string :as s]
            [clj-time.core :as t]
            [clj-time.format :as tf]
            [cascalog.api :as ca]
            [cascalog.ops :as co]
            [cascalog.more-taps :as cmt]))


;;
;; ## Log Types
;;
;; <pre>
;; "type": "Hover", "payload": {"itemId"": "22938"}
;; "type": "Play", "payload": {"itemId"": "24310e73", "marker": 60}
;; "type": "Home", "payload":
;;      {"popularItems": ["38989", "38725","8712", "5447", "14401"]
;;     , "recommendedItems": ["24865", "5794e23", "4704e23", "33473","22938"]
;;     , "recentItem": ["26342"]}
;; "type": "AddToQueue", "payload": {"itemId"": "18045"}
;; "type": "Rate", "payload": {"itemId"": "32212", "rating": 3}
;; "type": "Stop", "payload": {"itemId"": "6908", "marker": 7080}
;; "type": "Search", "payload": {"results": ["33856", "17554e1",...]}
;; "type": "Pause", "payload": {"itemId"": "19474", "marker": 2655}
;; "type": "Account", "payload": {"subAction": "parentalControls",
;;     "old": "adult", "new": "kid"}
;; "type": "Login"
;; "type": "Account", "payload": {"subAction": "updatePassword"}
;;  "type": "Queue",
;; "type": "Recommendations", "payload": {"recommendedItems":["38278", ...]}
;; "type": "ItemPage", "payload": {"itemId"": "11931"} 
;; "type": "Stop", "payload": {"itemId"": "11688", "marker": 300}
;; </pre>
;;
;; Play event logged every minutes 


;; ## Utils functions

;;
;; When t-uples are coming from delimited file all fields are of type string.
;; This function converts field into number
;; TODO can be extended to other types (Date...)
;; 
(defn ->number
  "Convert value to number"
  [v]
  (if (number? v) v (read-string v)))

(defn default
  "Give a default value if nil or empty string"
  [v d]
  (if (or (= "" v) (nil? v)) d v))

;; ## ETL and basic extraction
;;

(defn parse-json [line]
  (json/read-str line :key-fn keyword))

;;TODO Clean only when error occured
(defn clean-json
  "Ad hoc logic to clean broken json"
  [^String line]
  (.replace line "\"itemId\"\":" "\"itemId\":"))

(def raw-fields [:createdAt :sessionId :user :type :payload :userAgent :refId :auth])
(def fields-set (set raw-fields))
(def nullable-raw-fields [:auth :refId :payload])


(defn trimmer [^String s]
  (.toLowerCase (.replaceAll s "_" "")))

(defn corrector
  "Correct field name if contains same letters sequence"
  [fields]
  (let [s (set fields)
        m (into {} (map (juxt (comp trimmer name) identity) fields))]
    (fn [k]
      (get s k (m (trimmer (name k)) k)))))

(def correct-field (corrector (conj raw-fields :itemId)))

(defn clean-payload [p]
  (if (map? p)
    (into {} (map (fn [[k v]] [(correct-field k) v]) p))
    p))

(defn clean-log
  "Ad hoc logic to clean log map (ie mispelled or missing keys)"
  [log]
  (let [sk (set (keys log))]
    (if (= sk fields-set)
      log
      (let [init (into {} (map (fn [k] [k nil])
                               (clojure.set/difference fields-set sk)))]
        (into {}
             (map 
              (fn [[k v]] [(correct-field k)
                          (if (= :payload k) (clean-payload v) v)])
              log))))))

;; Add constraint on payload (:itemId required for some type of event) ??
(defn check-log
  "Checks minimal constraint on log structure , throw exception if not :
:sessionId :createdAt :user are non-nullable fields.
"
  [log]
  (let [log-miss? (fn [k] (nil? (k log)))
        missing (filter log-miss? [:sessionId :createdAt :user :type])
        ]
    (when (seq missing)
      (let [message (str "Missing Log Fields " (s/join " " missing))]
        (throw (RuntimeException. message))))
    true))

(defn fdefault [f d]
  (fn [a] (default (f a) d)))

(defn extractor [log]
  ;(mapv (fn [k] (k log "-"))  raw-fields)
  ((juxt :createdAt (fdefault :sessionId "-") (fdefault :user "-")
         :type :payload :userAgent :refId :auth) log))

(defn missing [& coll]
  (s/join ","
          (keep-indexed (fn [i e] (when (nil? e) (str (nth raw-fields i)))) coll)))

(defn missing-keys
  "Returns raw-fields missing in log. Returns nil if no missing."
  [log]
  (let [ks (set (filter (comp not nil? first) log))
        missing (clojure.set/difference fields-set ks)
        additional (clojure.set/difference ks fields-set)
        res (map name (sort (clojure.set/union missing additional)))]
    (when (seq res)
      (s/join "-" res))))


;;For test on repl
(defn ->logs [path & [start end]]
  (keep (fn [s] (try (parse-json s) (catch Throwable t nil)))
                (.split (slurp path) "\n")))

(def log-types [:Account :AddToQueue :Home :Hover :ItemPage :Login :Logout :Pause :Play :Position
                :Queue :Rate :Recommendations :Search :Stop :VerifyPassword :WriteReview])

(def log-type-fields (mapv #(str "?" (name %)) log-types))

;;some fields can be nullable
(def log-fields ["!creation" "?session" "?user" "!type"
                 "!payload" "!agent" "!ref" "!auth"])

(defn etl-logs [in & {:keys [trap] :or {trap (ca/hfs-textline "error")}}]
  (ca/<- log-fields
         (extractor ?clog :>> log-fields)
         (clean-json ?line :> ?json)
         (in ?line)
         (parse-json ?json :> ?log)
         (clean-log ?log :> ?clog)
         (:trap trap)))

(defn logs [in & {:keys [trap] :or {trap (ca/hfs-textline "error")}}]
  (ca/<- [?clog]
         (clean-json ?line :> ?json)
         (in ?line)
         (check-log ?clog :> true)
         (clean-log ?log :> ?clog)
         (parse-json ?json :> ?log)
         (:trap trap)))

(defn missing-keys-count [in]
  (ca/<- [?missing ?count]
         (co/count ?count)
         (missing-keys :< ?log :> ?missing)
         (in ?log)))

(defn missing-keys-logs [in]
  (ca/<- [?log]
         (missing-keys :< ?log :> ?missing)
         (in ?log)))

(defn type-count [in]
  (ca/<- [!type ?count]
         (co/count ?count)
         (in :>> log-fields)))

(defn missing-count [in]
  (ca/<- [?missing ?count]
         (co/count ?count)
         (missing :<< log-fields :> ?missing)
         (in :>> log-fields)))

(defn full-count [in]
  (ca/<- [?count]
         (co/count ?count)
         (in ?line)))

(defn user-count [in]
  (ca/<- [!user ?count]
         (co/count ?count)
         (in :>> log-fields)))

(def datetime-formatter (tf/formatters :date-time-no-ms))

(defn date->timestamp [s]
  (if s
    (try
     (/ (.getMillis (tf/parse datetime-formatter s)) 1000)
     (catch Throwable t -1))
    -1))

(defn expand-action
  "Extract payload information into a 2-uple."
  [type payload]
  (let [type (keyword type)]
    (cond (#{:Play :Pause :Stop} type)
          [(:itemId payload) (:marker payload)]
          (= :Rate type)
          [(:itemId payload) (:rating payload)]
          (#{:Hover :Stop :ItemPage :AddToQueue} type)
          [(:itemId payload) -1]
          (and (= :Account type) (= "parentalControls" (:subAction payload)))
          [(:old payload) (:new payload)]
          :else
          ["-" -1])))

(defn action-details [in]
  (ca/<- [?user ?session ?timestamp !type ?arg1 ?arg2]
         (date->timestamp !creation :> ?timestamp)
         (expand-action !type !payload :> ?arg1 ?arg2)
         (in :>> log-fields)))

(defn log-details [in]
  (ca/<- [?user ?session ?timestamp !type ?arg1 ?arg2]
         (date->timestamp !creation :> ?timestamp)
         (expand-action !type !payload :> ?arg1 ?arg2)
         (extractor ?log :>> log-fields)
         (in ?log)))

(defn play-duration
  "Query to compute the duration of a play"
  [in]
  (ca/<- [?user ?session ?item ?duration1 ?duration2 ?min-t ?min-m ?max-m]
         (- ?max-t ?min-t :> ?duration1)
         (* 60 ?count-plays :> ?duration2)
         (read-string ?marker :> ?mk)
         (read-string ?timestamp :> ?ts)
         (co/max ?ts :> ?max-t)
         (co/min ?ts :> ?min-t)
         (co/max ?mk :> ?max-m)
         (co/min ?mk :> ?min-m)
         (co/count :> ?count-plays)
         (= !type "Play")
         (in ?user ?session ?timestamp !type ?item ?marker)))

(ca/defaggregateop log-counts
  ([] {})
  ([context val] (when val (update-in context [(keyword val)] (fnil inc 0))))
  ([context] [(mapv (fn [t] (t context 0)) log-types)]))

(defn action-counts
  [in]
  (let [out-vars (vec (concat ["?user" "?session" "?total" ] log-type-fields))]
   (ca/<- out-vars
          (co/count ?total)
          (log-counts !type :>> log-type-fields)
          (in ?user ?session ?timestamp !type ?arg1 ?arg2))))

#_(defn film-counts
  "Gets distinct films count played rated"
  [in]
  (in ?user ?session ?timestamp ?type ?arg1 ?arg2))


;; ## Kid / Adult Classification
;; 
(defn kid-user
  "List all kid users with start and end timestamp (-1 means unbounded)"
  [in]
  (ca/<- [?user ?start-ts ?end-ts]
         (default !!start-ts -1 :> ?start-ts)
         (default !!end-ts -1 :> ?end-ts)         
         ((ca/<- [?user ?timestamp]
                 (= ?arg1 "kid")
                 (in ?user ?session ?timestamp !type ?arg1 ?arg2))
          :> ?user !!end-ts)
         ((ca/<- [?user ?timestamp]
                 (= ?arg2 "kid")
                 (in ?user ?session ?timestamp !type ?arg1 ?arg2))
          :> ?user !!start-ts)))
;;
;; 
;;
(defn between?
  "Returns true if start < ts < end.
Not bounded if start or end are nil or negative"
  [ts start end]
  (let [start (if start start -1)
        end (if end end -1)]
    (cond (nil? ts)
         false
         (= start end -1)
         true
         (neg? end)
         (> ts start)
         (neg? start)
         (> end ts)
         :else
         (> end ts start))))

(defn kid-films [kid-user-in in]
  (ca/<- [?arg1 ?user-count]
         (co/distinct-count ?user :> ?user-count)
         ;;(:distinct true)
         (= !type "Play")
         (between? ?ts ?start-ts ?end-ts)
         ((co/each ->number) ?timestamp ?start ?end :> ?ts ?start-ts ?end-ts)
         (in ?user !session ?timestamp !type ?arg1 ?arg2)
         (kid-user-in ?user ?start ?end)))

(ca/defbufferiterop distinct-counts
  "Returns distinct counts"
  [tuples-iter]
  (let [{s1 true s2 false}
        (reduce
         (fn [a [i b]] (update-in a [b] conj i))
         {false #{} true #{}} 
         (iterator-seq tuples-iter)
         )
        c1 (count s1)
        c2 (count s2)]
    [[(+ c1 c2) c1]]
   ))


(defn films-split [kid-films plays]
  (ca/<- [?user ?film-count ?kid-film-count]
         (distinct-counts :<< [?item ?is-kid-film] :>> [?film-count ?kid-film-count])
         (kid-films ?item ?total :> ?is-kid-film)
         (plays ?user ?session ?item ?duration1 ?duration2 ?min-t ?min-m ?max-m)))

(defn kidadult-classify
  "Retuns query (user,kid/adult flag,film count,kid film count)
given kid films, and viewed films"
  [kid-films plays & {:keys [lower] :or {lower 0.6}}]
  (ca/<- [?user ?kid ?film-count ?kid-film-count]
         (> ?kid-film-prop lower :> ?kid)
         (ca/div ?kid-film-count ?film-count :> ?kid-film-prop)
         (distinct-counts :<< [?item ?is-kid-film] :>> [?film-count ?kid-film-count])
         (kid-films ?item ?total :> ?is-kid-film)
         (plays ?user ?session ?item ?duration1 ?duration2 ?min-t ?min-m ?max-m)))

;;
;; ## Session clustering
;;
;; Compute session attributes :
;;    - session duration.
;;    - count distinct viewed films.
;;    - count of other actions (Account,Pause,Stop Play Home Hover, Rate,Search ,...).
;;

(defn update-map [m [k v]]
  (update-in m [k] (fnil conj []) v))

(defn group-by-first [coll]
  (reduce
   (fn [a [t i]] (update-in a [t] (fnil conj []) i))
   {} 
   coll)) 

(defn next-min-max [[a b] t]
  (if (and a b)
    [(if (> a t) t a) (if (> t b) t b)]                    
    [t t]))

(ca/defbufferiterop session-stats-
  [tuples-iter]
  (let [tuples (iterator-seq tuples-iter)
        [ms groups]
        (reduce (fn [[a b] [t & bs]] [(next-min-max a t) (update-map b bs)])
         [[nil nil] {}] tuples)
        res (map #(count (get groups %)) ["Account" "Hover" "Rate" "Pause"])
        c (->> "Play" (get groups) distinct count)]
    [(vec (concat ms res [c (count tuples)]))]))

(defn session-stats
  "Session stats query from action details source"
  [in]
  (ca/<- [?user ?session ?duration ?acct ?hover ?rate ?pause ?play ?count-logs]
         (- ?max-t ?min-t :> ?duration)
         (->number ?timestamp :> ?ts)
         (session-stats-
          :<< [?ts ?type ?arg1]
          :>> [?min-t ?max-t ?acct ?hover ?rate ?pause ?play ?count-logs])
         (in ?user ?session ?timestamp ?type ?arg1 ?arg2)))

;; ## Films Recommendations
;;
;; Item-based recommendations takes as normal approach the ratings (here too limited)
;; Viewed films allows to link films and users :
;;   - (user, session,played film) --> (film1, film2 , user/session) 
;;   - discard relation if play duration is not low 
;;   - take care of film episodes 1234e34 
;;


;; count distinct films played, rated, distinct
;; use defaggregateop to count ratin

(def item-actions
  ;(sorted-set "Play" "Rate")
  {"Play" 0 "Rate" 1}
  )
(defn in-type? [t]
  (not (nil? (item-actions t))))

;; support 1-uple of map ??
(ca/defaggregateop count-by-key
  ([] (into {} (map vector item-actions (repeat 0))))
  ([state key] (update-in state [key] (fnil inc 0)))
  ([state] [state]))

(ca/defaggregateop count-by-item-actions
  ([] (vec (repeat (count item-actions) 0)))
  ([state key] (if-let [v (item-actions key)]
                 (update-in state [v] inc)
                 state))
  ([state] [state]))

(defn trim-item
  "Remove episode component if existing"
  [id]
  (let [id (str id)
        i (.indexOf id "e")]
    (if (> i -1)
      (.substring  id 0 i)
      id)))

(defn ratings
  "Query returning all ratings present in logs. Episodes are merged into one entity"
  [in]
  (ca/<- [?user ?item ?rating]
         (trim-item ?item_ :> ?item)
         (in ?user ?session ?timestamp "Rate" ?item_ ?rating)))

;; This implementation based on primitive array does NOT work
;; java.lang.ClassCastException: java.lang.Long cannot be cast to clojure.lang.IFn
#_(ca/defaggregateop count-by-item-actions
  ([] (long-array (count item-actions)))
  ([state key] ((if-let [v (item-actions key)]
                  (aset state v (inc (aget state v))))
                state))
  ([state] [state]))

(defn item-stats [in]
  (let [item-user-q (ca/<- [?item ?user !type]
                           (in-type? !type :> true)
                           (in ?user ?session ?timestamp !type ?item ?arg2)
                           )
        ] (ca/<- [?item ?viewed-user-ct ?rated-user-ct]
                 (count-by-item-actions  ?type :>  ?viewed-user-ct ?rated-user-ct)
                 (item-user-q ?item ?user ?type))))

(defn item-ratings-stats
  "Query to count how many items was rated
given count of users who rated same film and rating. "
  [in]
  (let [item-q
         (ca/<- [?item ?rating ?user-count]
               (co/count ?user-count)
               (in ?user ?item ?rating))
        item-freq
        ;;
        (ca/<- [?user-count ?rating ?item-total]
               (co/count ?item-total)
               (item-q _ ?rating ?user-count))
        ]
    item-freq))

(defn user-ratings-stats
  "Query to count how many user rated given rated item count and rating."
  [in]
  (let [user-q
        (ca/<- [?user ?rating ?item-count]
               (co/distinct-count ?item :> ?item-count)
               (in ?user ?item ?rating))
        user-freq
        (ca/<- [?item-count ?rating ?user-total]
               (co/count ?user-total)
               (user-q _ ?rating ?item-count))
        ]
    user-freq))

(defn pairs
  "Returns all pairs made from elements of coll"
  [coll]
  (let [s (set coll)]
    (for [a s b s :when (< (compare a b) 0)] [a b])))

;; (defparallelagg dosum :init-var #'identity :combine-var #'+)
(ca/defbufferiterop distinct-pairs
  "Returns all distinct pairs of the first element of the tuples"
  [tuples-iter]
  (->> (iterator-seq tuples-iter)
        (map first)
        pairs
        vec))

(defn items-relation
  ""
  [in]
  (ca/<- [?user ?session ?item1 ?item2 ]
         (distinct-pairs :<< [?item] :>> [?item1 ?item2])
         (> ?duration 60)
         (->number ?duration1 :> ?duration)
         (in ?user ?session ?item ?duration1 ?duration2 ?min-t ?min-m ?max-m)
         ))

(defn -main [dir out]
  (let [log-stage (str out "/log")
        details-stage (str out "/details")
        trap (str out "/error")]
   (ca/?- "Full count"
          (cmt/lfs-delimited (str out "/total") :sinkmode :replace)         
          (full-count (ca/lfs-textline dir)))
   (ca/?- "ETL log"
          (ca/lfs-seqfile log-stage :sinkmode :replace)
          (etl-logs (ca/lfs-textline dir) :trap (ca/lfs-textline trap)))
   (ca/?- "Type count"
          (cmt/lfs-delimited (str out "/type") :sinkmode :replace)         
          (type-count (ca/lfs-seqfile log-stage)))
   (ca/?- "Missing count"
          (cmt/lfs-delimited (str out "/missing") :sinkmode :replace)         
          (missing-count (ca/lfs-seqfile log-stage)))
   (ca/?- "action details"
          (cmt/lfs-delimited details-stage :sinkmode :replace)
          (action-details (ca/lfs-seqfile log-stage)))
   (ca/?- "action counts"
          (cmt/lfs-delimited (str out "/action") :sinkmode :replace)
          (action-counts (cmt/lfs-delimited details-stage)))
   (ca/?- "session stats"
          (cmt/lfs-delimited (str out "/session") :sinkmode :replace)
          (session-stats (cmt/lfs-delimited details-stage)))
   (ca/?- "play duration"
          (cmt/lfs-delimited (str out "/play") :sinkmode :replace)
          (play-duration (cmt/lfs-delimited details-stage)))
   (ca/?- "kids"
          (cmt/lfs-delimited (str out "/kid") :delimiter "," :sinkmode :replace)
          (kid-user (cmt/lfs-delimited details-stage)))
   (ca/?- "kid films"
          (cmt/lfs-delimited (str out "/kidfilm") :sinkmode :replace)
          (kid-films (cmt/lfs-delimited (str out "/kid") :delimiter ",")
                    (cmt/lfs-delimited details-stage)))
   (ca/?- "items relation"
          (cmt/lfs-delimited (str out "/itemrel") :sinkmode :replace)
          (items-relation (cmt/lfs-delimited (str out "/play"))))))
