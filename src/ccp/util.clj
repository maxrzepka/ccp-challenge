(ns ccp.util
  (:require [clojure.string :as s]
            [cascalog.more-taps :as cmt]
            [cascalog.api :as ca]))

;;
;; # Some misc functions
;;

(defn class-methods
  [clazz]
  (let [b (bean clazz)]
    (apply merge
           (cons (into {} (for [method (:declaredMethods b)
                           :let [name (.getName method)
                                 params (vec (.getParameterTypes method))]]
                       [name params]))
            (map methods (:interfaces b))))))

;;
;; # IO Functions
;;

(defn file->coll
  "Converts file content into list of rows"
  [path & {:keys [delimiter converter]
                          :or {delimiter "," converter identity}}]
  (mapv (fn [r] (mapv converter (.split r delimiter))) (.split (slurp path) "\n")))

(defn coll->file
  "Dumps list of rows into file"
  [coll out & {:keys [delimiter] :or  {delimiter ","}}]
  (with-open [wrt (clojure.java.io/writer out)]
    (doseq [r coll]
      (.write wrt (str (s/join delimiter r) "\n")))))

;;
;; # Hadoop-oriented Functions
;;

(defn dfs?
  "Detects if path belongs to a distributed file system"
  [^String path]
  (some #(.startsWith path %) ["hdfs:" "s3:" "s3n"]))

(defn tap
  "Returns appropriate cascading tap."
  [mode format ]
  (let [mode (keyword mode) format (keyword format)
        taps {:local {:delimited cmt/lfs-delimited
                      :seq ca/lfs-seqfile
                      :text ca/lfs-textline}
              :dist  {:delimited cmt/hfs-delimited
                      :seq ca/hfs-seqfile
                      :text ca/hfs-textline}}
        modes (-> taps keys set)
        formats (-> taps :local keys set)]
    (assert (modes mode) (str mode " unknown mode, only supported " (seq modes)))
    (assert (formats format)
            (str format " unknow format, only supported " (seq formats)))
    (get-in taps [mode format])))
