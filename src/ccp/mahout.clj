(ns ccp.mahout
  (:use [ccp core util])
  (:require [cascalog.more-taps :refer [lfs-wrtseqfile]]
            [cascalog.api :as ca]
            [cascalog.ops :as co]
            [cascalog.more-taps :as cmt]
            )
           ;; Mahout clustering
  (:import [org.apache.mahout.clustering.canopy CanopyDriver]
           [org.apache.mahout.clustering.kmeans KMeansDriver]
           [org.apache.mahout.clustering Cluster]
           [org.apache.mahout.clustering.classify WeightedVectorWritable]
           [org.apache.mahout.math NamedVector DenseVector Vector VectorWritable]
           [org.apache.mahout.common HadoopUtil]
           [org.apache.mahout.common.distance
            TanimotoDistanceMeasure
            CosineDistanceMeasure
            ManhattanDistanceMeasure]
           [org.apache.mahout.utils.clustering ClusterDumper]
           ;; Mahout recommandation
           [ org.apache.mahout.cf.taste.common TasteException]
           [org.apache.mahout.cf.taste.similarity UserSimilarity]           
           [org.apache.mahout.cf.taste.impl.similarity
            LogLikelihoodSimilarity
            TanimotoCoefficientSimilarity
            PearsonCorrelationSimilarity
            ]
           [org.apache.mahout.cf.taste.impl.neighborhood NearestNUserNeighborhood]
           [org.apache.mahout.cf.taste.recommender Recommender]
           [org.apache.mahout.cf.taste.impl.recommender
            GenericUserBasedRecommender GenericItemBasedRecommender]
           [org.apache.mahout.cf.taste.eval  RecommenderBuilder DataModelBuilder]
           [org.apache.mahout.cf.taste.impl.eval AverageAbsoluteDifferenceRecommenderEvaluator]
           [org.apache.mahout.cf.taste.model DataModel]
           [org.apache.mahout.cf.taste.impl.common FastByIDMap]
           [org.apache.mahout.cf.taste.impl.model GenericDataModel]           
           [org.apache.mahout.cf.taste.impl.model.file FileDataModel]
           ;; Hadoop 
           [org.apache.hadoop.fs Path]
           [org.apache.hadoop.conf Configuration]
           [org.apache.hadoop.io Text IntWritable]
           ;; Misc
           [java.io File]
           ))

;;
;; ## Misc Mahout/clj functions
;;

(defn ->Vector
  "Convert a coll into a Mahout Vector"
  [coll]
  (DenseVector. (double-array (map ->number coll))))

;;
;; Use NamedVector to retain IDs when clustering 
;; code from http://stackoverflow.com/questions/11848038/how-to-read-mahout-clustering-output/11853826#11853826
;;
(defn ->VectorWritable [name & args]
  (let [v (VectorWritable.)]
    (.set v (NamedVector. (->Vector args) name))
    v))

(defn ->name [vector]
  (str (.getName ^NamedVector vector)))

(defn extract-name [wrtvector]
  (->name (.getVector wrtvector)))

(defn ->Text [s]
  (Text. s))

(defn stats->vector
  "Query to convert Session stats into mahout-compliant output for clustering
ie t-uple (Text,VectorWritable) "
  [in]
  (ca/<- [?session-text ?vector]
         (->Text ?session :> ?session-text)
         (->VectorWritable ?session ?duration ?acct ?hover ?rate ?pause ?play ?count-logs
                           :> ?vector)
         (in ?user ?session ?duration ?acct ?hover ?rate ?pause ?play ?count-logs)
         ))

(defn vector-seqfile
  "Dump (Text , VectorWritable) query to a Writable Seq File"
  [q out & {:keys [mode] :or {mode :lfs}}]
  (let [wrtseqfile ({:hfs cmt/hfs-wrtseqfile} mode cmt/lfs-wrtseqfile)]
    (ca/?- (wrtseqfile out Text VectorWritable
                       :outfields (ca/get-out-fields q) :sinkmode :replace)
          q)))


;;
;; ## kmeans Clustering
;;

(defn cluster->csv
  "Convert cluster results into csv text format (key, cluster-id)"
  [in out]
  (let [wrtseqfile (if (dfs? in) cmt/hfs-wrtseqfile cmt/lfs-wrtseqfile)
        delimited (if (dfs? out) cmt/hfs-delimited cmt/lfs-delimited)]
    (ca/?<-
     (delimited out :sinkmode :replace :delimiter ",")
     [?id ?cluster]
           (str ?key :> ?cluster)
           (extract-name ?vector :> ?id)
           ((wrtseqfile (str in "/" Cluster/CLUSTERED_POINTS_DIR)
                        IntWritable WeightedVectorWritable
                        :outfields ["?key" "?vector"]) ?key ?vector))))
 
(defn find-centroid
  "Find initial centroids using canopy methods"
  [in out & {:keys [t1 t2] :or {t1 0.7 t2 0.2}}]
  (assert (> t1 t2) (str "T1=" t1 " shoud be greater than T2=" t2))
  (CanopyDriver/run (Path. in) (Path. out) (TanimotoDistanceMeasure.)
                    (float t1) (float t2) true 0.0 false))

(defn kmeans
  "Run k-means clustering given initial cluster"
  [in cluster out]
  (KMeansDriver/run (Path. in) (Path. cluster) (Path. out)
                    (TanimotoDistanceMeasure.)
                    (double 0.1) 100 true (double 0) false))

(defn kmeans-cluster
  "Compute kmeans with initial centroid computed via canopy method.
in : "
  [in out t1 t2 & {tmp :tmp :or {tmp "/tmp/clustering"}}]
  (let [canopy-tmp (str tmp "/canopy" t1 "_" t2)]
    (do
      (println "Find canopy centroids with t1=" t1 " t2=" t2)
      (HadoopUtil/delete nil (into-array [(Path. canopy-tmp)]))
      (find-centroid in canopy-tmp :t1 t1 :t2 t2)
      (println "Run kmeans...")
      (HadoopUtil/delete nil (into-array [(Path. out)]))
      (kmeans in (str canopy-tmp "/clusters-0-final") out))))

(defn run-clustering
  "Performs clustering given features input and outputs csv file.
Optional : t1 t2 canopy parameter by default 0.7 0.3"
  [in out
   & {:keys [tmp mode t1 t2] :or {t1 0.7 t2 0.3 mode :lfs tmp "/tmp/clustering"}}]
  (let [vec-path (str tmp "/vector")
        cluster-path (str tmp "/cluster")
        t (tap (if (dfs? in) :dist :local) :delimited)]
    (do
      (vector-seqfile (stats->vector (t in)) vec-path :mode mode)
      (kmeans-cluster vec-path cluster-path t1 t2)
      (cluster->csv cluster-path out))))

;;
;; ## Misc Functions
;; 
(defn extension
  "Extracts extension of a file"
  [filename]
  (.substring filename (inc (.lastIndexOf filename "."))))

(defn dump-cluster
  "Dumps cluster output in various formats csv, text, graphml."
  [cluster point out & [ext]]
  (let [ext (if ext ext (extension out))
        l (first (.toUpperCase (str ext)))
        ext ({\T "TEXT" \C "CSV" \G "GRAPH_ML"} l "TEXT")]
    (.run (ClusterDumper.)
         (into-array String ["-i" cluster "-p" point "-o" out "-of" ext]))))

(defn rows->Vector [coll]
  (map ->Vector coll))

(comment
 (def b (map #(DenseVector. (double-array %)) a))
 (def a (map #(->> (.split % "\t") next next (map ->number))
             (.split (slurp "data/session_test.txt") "\n")))

 (def t (TanimotoDistanceMeasure.))
 (map #(.distance t (first b) %) (rest b)))


;;
;; ## recommendation system
;;
;; starting point https://groups.google.com/d/msg/clojure/bgH9EIwy1wE/HgVYSbYHbtYJ

(defn evaluate [recommender model]
  (let [builder (reify RecommenderBuilder (buildRecommender [_ _] recommender))
        mbuilder (reify DataModelBuilder
                      (buildDataModel [this trainingdata]
                        (GenericDataModel. (GenericDataModel/toDataMap trainingdata))))
        evaluator (AverageAbsoluteDifferenceRecommenderEvaluator.)]
    (.evaluate evaluator builder nil model 0.9 0.1)))

(defn build-model [file]
  (FileDataModel. (File. file)))

(defn build-recommender [infile similarity n]
  (let [ss #{:pearson :tanimoto :loglikehood}
        tt #{:user :item}]
    (assert (ss similarity) (str "Similarities available : " ss))
   (let [m (build-model infile)
         s (condp = similarity
             :loglikehood (LogLikelihoodSimilarity. m)
             :tanimoto (TanimotoCoefficientSimilarity. m)
             :pearson (PearsonCorrelationSimilarity. m))
         n (NearestNUserNeighborhood. n s m)
         r (GenericUserBasedRecommender. m n s)
         ]
     r)))

(defn evaluate-recommender [infile similarity n]
  (evaluate (build-recommender infile similarity n) (build-model infile)))

(defn estimate-rate
  "Given a list of (user,item) appends the recommendation (user,item,rating)"
  [recommender coll]
  (for [[user item] coll]
    [user item (.estimatePreference recommender user item)]))

(defn find-best-recommender [infile]
  (for [s [:pearson :tanimoto :loglikehood] i (range 9)
        :let [ n (int (Math/pow 2 i))]]
    [s n (evaluate-recommender infile s n)]))

;; from http://mail-archives.apache.org/mod_mbox/mahout-user/201207.mbox/%3CCAOyMWziV_mBX6Jv88+E-d+0c1PO0OV1y_YJv2CO8wChAtt0YqQ@mail.gmail.com%3E
#_(def prefs-file "data.txt")
#_(org.apache.mahout.common.RandomUtils/useTestSeed)
#_(let [model (FileDataModel. (File. prefs-file))
      evaluator (GenericRecommenderIRStatsEvaluator.)
      rbuilder (reify RecommenderBuilder
                 (buildRecommender [this model]
                   (let [sim (LogLikelihoodSimilarity. model)]
                     (GenericBooleanPrefItemBasedRecommender. model sim))))
      mbuilder (reify DataModelBuilder
                 (buildDataModel [this training-data]
                   (GenericBooleanPrefDataModel.
                    (GenericBooleanPrefDataModel/toDataMap training-data))))]
  (prn (.evaluate evaluator rbuilder mbuilder model nil 10
                  GenericRecommenderIRStatsEvaluator/CHOOSE_THRESHOLD
                  0.01)))



