CCP Challenge
=============

Introduction
------------

Full Details <https://ccp.cloudera.com/display/cert/Home> with links to
datasets

Setup on Amazon Web Service (AWS)
---------------------------------

Here the following steps :

### CDH Cluster on EC2

Instructions based on [Create a CDH Cluster on Amazon EC2 via Cloudera
Manager](http://blog.cloudera.com/blog/2013/03/how-to-create-a-cdh-cluster-on-amazon-ec2-via-cloudera-manager/)
Cloudera Manager has a new version different from the one described in
the article.

Warning : CDH is installed on a instance running with the user ubuntu

Other
[re](http://www.thecloudavenue.com/2013/04/setting-up-cdh-cluster-on-amazon-ec2-in.html)source.

### Datasets on S3

-   Download locally the datasets from cloudera portal (authentification
    required)
-   Upload to S3 through AWS console

### Elastic MapReduce (EMR)

From AWS console with the following parameters :

-   custom jar : s3n://ccp-emr/ccp.jar
-   arguments : ccp.job.ETLStats s3n://ccp-challenge/small
    s3n://ccp-emr/small
-   log : s3n://ccp-emr/log

To launch a cluster on EMR with [lein-emr
](https://github.com/dpetrovics/lein-emr): \`lein emr -n "CCP ETL lein"
-t "large" -s 5 -b 0.2 -bs bsaconfig.xml\`

This plugin uses command-line tool elastic-mapreduce. More details about
elastic-mapreduce
[here](http://sujee.net/tech/articles/hadoop/amazon-emr-beyond-basics/).

Connect to the host with the user hadoop : \`ssh -i \<path to perm
file\> hadoop@hostname\` Run the job manually on the EMR host : hadoop
jar ccp.jar ccp.job.ETLStats s3n://AWS-ACCESS:AWS-SECRET@ccp-challenge
s3n://AWS-ACCESS:AWS-SECRET@ccp-emr

Access s3 files explained
[here](http://www.datasciencecentral.com/profiles/blogs/s3-as-input-or-output-for-hadoop-mr-jobs),
AWS Keys can also be defined in conf/hdfs-site.xml.

Tools
-----

-   Mahout Myrrix
    <http://www.quora.com/Apache-Mahout/What-are-the-pros-cons-of-using-Apache-Mahout-when-creating-a-real-time-recommender-system>
