(defproject ccp-challenge "0.1.0-SNAPSHOT"
  :description "2013 Cloudera Data Science Challenge"
  :url "https://github.com/maxrzepka/"
  :uberjar-name "ccp.jar"
  :target-path "dist"
  :aot [ccp.core, ccp.job]
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/tools.logging "0.2.6"]
                 [cascalog "1.10.2"]
                 [cascalog/cascalog-lzo "1.10.1-SNAPSHOT"] 
                 [cascalog/cascalog-checkpoint "1.10.2-SNAPSHOT"]
                 [org.apache.mahout/mahout-core "0.8"]
                 [org.apache.mahout/mahout-math "0.8"]
                 [org.apache.mahout/mahout-integration "0.8"]
                 ;;TODO find correct log4j exclusions
                 ;;Warning: Class path contains multiple SLF4J bindings
                 ;; :exclusions [org.slf4j/slf4j-log4j12 log4j/log4j]
                 [net.myrrix/myrrix-client "1.0.1"]
                 [net.myrrix/myrrix-online "1.0.1"]
                 ;[net.myrrix/myrrix-online-local "1.0.1"]
                 [org.clojure/data.json "0.2.2"]
                 [clj-time "0.5.1"]
                 ]
  :exclusions [org.apache.hadoop/hadoop-core
               org.clojure/clojure]
  :profiles {:dev {:dependencies [[cascalog/midje-cascalog "1.10.1"]
                                  [org.apache.hadoop/hadoop-core "1.1.2"]
                                  [org.apache.httpcomponents/httpclient "4.3"]
                                  ]
                   :plugins [[lein-midje "3.0-beta1"]
                             [lein-emr "0.2.0-SNAPSHOT"]]}
             :provided {:dependencies [[org.apache.hadoop/hadoop-core "0.20.2-dev"]]}}
  )
